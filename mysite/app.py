#!/usr/bin/env python3


from flask import Flask

zorro = Flask(__name__)

# Decorators have operator --> @
# math addition Opertator +

@zorro.route('/')
def index():
    return("HELLO FROM ZORRO")

@zorro.route('/add/<x>/<y>',methods=['GET','POST'])
def add(x,y):
    return str(int(x)+int(y))

@zorro.route('/home')
def  welcom():
    return("welcome")

@zorro.route('/contact')
def contact():
    return("hereis your contacts list: ")

@zorro.route('/links')
def links():
    return("here is your links: ")

@zorro.route('/calc/<w>/<z>',methods=['GET','POST'])
def calculator(w,z):
    return str(int(w)*int(z))

@zorro.route('/about')
def about():
    return("Welcome to about page")

@zorro.route('/home')
def say():
    return("Welcom home")


zorro.run()
