
#!/usr/bin/env python3

# Write a program that asks a user to type their age in years, and prints back the age in months.


age = int(input("please enter your age: "))
print(age*12)


# Write a program that asks a user to type their age in months, and prints back the age in years.


age = int(input("please enter your age: "))
print(age/12)


# Write a program that asks the user to select a number and prints the word Boom if the number is divisible by 7

x = int(input("Enter a number: "))
if x%7==0:
    print("%d boom"%x)
else:
    print("%d is not divisible by 7"%x)


# Write a program that asks the user to select a number and prints the word Boom if the number is divisible by 7 or contains the number 7.

x = int(input("Enter a number: "))
if x%7==0:
    print("%d boom"%x)
else:
    print("%d is not divisible by 7"%x)


# Write a program that asks the user to choose three numbers and prints the largest one between them.

number1 = int(input('Enter First number : '))
number2 = int(input('Enter Second number : '))
number3 = int(input('Enter Third number : '))
lst = [number1, number2, number3]
print("The largest of the 3 numbers is : ", max(lst))
print("The smallest of the 3 numbers is : ", min(lst))


# Then write a program that asks the user to select the first organ, the difference and the number of organs in an invoice series and prints the series sum.

a = 0
b = int(input("enter first number : "))
c = int(input("enter second  number : "))
d = int(input("enter third number : "))
for i in range(b,d,c):
    a =  a + i
print(a)
